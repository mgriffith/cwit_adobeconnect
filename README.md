# CWIT_AdobeConnect #

This repository houses FAQs for the Adobe Connect installation at the University of Arizona.

### What is this repository for? ###

* Documentation, built in MKDocs, to assist the faculty and students at the UA in using Adobe Connect

### Who do I talk to? ###

* The Campus-Wide Instructional Technology SIG, part of LATTe, is responsible for the content on these pages.
* mgriffith is responsible for maintaining the MKDocs generated site.